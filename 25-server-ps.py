# /usr/bin/python
#-*- coding: utf-8-*-
#  
# ---------------------------------------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# Descripcio : 25-ps.py
# ---------------------------------------------------------------------
import sys,socket,argparse
import time,datetime
from subprocess import Popen, PIPE

ts = time.time()

parser = argparse.ArgumentParser(description="""Exemple rep server""")
parser.add_argument("-p","--port",type=int,default=50001)
args=parser.parse_args()

HOST = ''
PORT = args.port

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
s.listen(1)
while True:
	conn, addr = s.accept()
	print "Connected by", addr
	st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d_%H:%M:%S')
	nom = addr[0] + '_'+ str(addr[1])+'_'+st+'.log' 
	f = open(nom,'w')
	while True:
		data = conn.recv(1024)
		if not data: break
		print data
		f.write(data)
	f.close()
	conn.close()
sys.exit(0)
