# /usr/bin/python
#-*- coding: utf-8-*-
#
# 26-telnet-server.py
# ---------------------------------------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# Descripcio : 
# ---------------------------------------------------------------------
import sys,socket,os,signal,argparse
from subprocess import Popen, PIPE

parser = argparse.ArgumentParser(description="""server telnet""")
parser.add_argument("-p","--port",type=int, default=50001)
parser.add_argument("-d","--debug",type=int, default=1)
args=parser.parse_args()

HOST = ''
PORT = args.port

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
s.listen(1)
while True:
	conn, addr = s.accept()
	print "Connected by", addr
	while True:
		command=conn.recv(1024)
		if not command: break
		pipeData = Popen(command,shell=True,stdout=PIPE,stderr=PIPE)
		for line in pipeData.stdout: 
			conn.send(line)
		conn.send(chr(4))
	conn.close()
s.close()
sys.exit(0)



















