#!/usr/bin/env python
# -*- coding: utf-8 -*-
#@ edt asix m06 curs 2018-2019
# nom : Franlin colque
# curs : hisx2
#exercici : 18-fork-signal.py 
#-----------------------------------------------------------------------
import sys,os,signal

def mysigusr1(signum,frame):
	print "Signal handler called with signal:", signum
	print "Hola Radiola"

def mysigusr2(signum,frame):
	print "Signal handler called with signal:", signum
	print "Hasta luego lucas"
	sys.exit(1)

print "Hola, començament del programa principal"
print "PID pare: ", os.getpid()
#crea fills
pid=os.fork()
if pid !=0:
  #os.wait()
  print "Programa Pare", os.getpid(), pid
  print "Hasta lugo lucas!"
  sys.exit(0)
  #el programa fill esta en marcha no mort,
print "Programa fill", os.getpid(), pid
signal.signal(signal.SIGUSR1,mysigusr1)
signal.signal(signal.SIGUSR2,mysigusr2)
while True:
	pass
signal.alarm(0)
sys.exit(0)
