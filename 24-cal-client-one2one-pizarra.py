# /usr/bin/python
#-*- coding: utf-8-*-
#
# 24-cal-client-one2one-pizarra.py  
# ---------------------------------------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# nom : Franlin colque
# curs : hisx2
#Descripcio: Quan es conecta al server rep la data y tanca la connexio
# ----------------------------------------------------------------
import sys,socket,argparse

parser = argparse.ArgumentParser(description="""Exemple CAL server""")
parser.add_argument("-s","--server",type=str,default="")
parser.add_argument("-p","--port",type=int,default=50001)
args=parser.parse_args()

HOST = args.server
PORT = args.port

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))
while True:
	data = s.recv(1024)
	if not data: break
	print 'Data:', str(data)
s.close()
sys.exit(0)
