# /usr/bin/python
#-*- coding: utf-8-*-
#
# exemple-echoClient.py  
# ------------------------------------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
#[isx27423760@i16 ipc-2018-19]$ telnet localhost 50001
#[isx27423760@i16 ipc-2018-19]$ ncat localhost 50001
#[isx27423760@i16 ipc-2018-19]$ ps | ncat localhost 50001
#[isx27423760@i16 ipc-2018-19]$ ncat localhost 50001 < /etc/passwd
# -------------------------------------------------------------------
import sys,socket
HOST = ''
PORT = 50001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#El lligam
s.bind((HOST,PORT))
#Escolta 
s.listen(1)
#i el programa servidor es quedara clavat , retorna una tupla(conexio,adreca cliet)
conn, addr = s.accept()
print "Connected by" , addr
#cuan el client tanca la conexio , exit
while True:
	#reps el msg
	data = conn.recv(1024)
	#quan es tanqui la connexio
	if not data: break
	#retornas el msg
	conn.send(data)
conn.close()
sys.exit(0)


