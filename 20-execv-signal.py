#!/usr/bin/env python
# -*- coding: utf-8 -*-
#@ edt asix m06 curs 2018-2019
# nom : Franlin colque
# curs : hisx2
#exercici : 20-exec-fork.py
#-----------------------------------------------------------------------
import sys,os,signal
print "Hola, començament del programa principal"
print "PID pare: ", os.getpid()

pid=os.fork()
if pid !=0:
  print "Programa Pare", os.getpid(), pid
  print "Hasta lugo lucas!"
  sys.exit(0)

print "Programa fill", os.getpid(), pid
os.execv("/usr/bin/python",["/usr/bin/python","16-signal.py","200"])
print "Hasta lugo lucas!"
sys.exit(0)
