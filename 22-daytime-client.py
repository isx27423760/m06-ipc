# /usr/bin/python
#-*- coding: utf-8-*-
#
# 22-daytime-client.py  
# ---------------------------------------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# nom : Franlin colque
# curs : hisx2
#Descripcio: Quan es conecta al server rep la data y tanca la connexio
# ----------------------------------------------------------------
import sys,socket
HOST = ''
PORT = 50001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))
fecha = s.recv(1024)
s.close()
print 'Received', repr(fecha)
sys.exit(0)
