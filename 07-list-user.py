#!/usr/bin/env python
# -*- coding: utf-8 -*-
#@ edt asix m06 curs 2018-2019
# nom : Franlin colque
# curs : hisx2
#exercici : 7 
# descripcio : EXemple de creacio de objectes i funcions per a users
#----------------------------------------------------------------------------
import argparse,sys

class UnixUser():
	"""Classe UnixUser: prototipus de /etc/passwd
	login:passwd:uid:gid:gecos:homeDirectory:shell"""
	def __init__(self,userLine):
		"""constructor objectes UnixUser"""
		userFile=userLine.split(":")
		#camps
		self.login=userFile[0]
		self.password=userFile[1]
		self.uid=int(userFile[2])
		self.gid=int(userFile[3])
		self.gecos=userFile[4]
		self.home=userFile[5]
		self.shell=userFile[6]
	def __str__(self):
		"""functió to_string"""
		return "%s %s %d %d %s %s %s" % (self.login, self.password, self.uid, \
		self.gid, self.gecos, self.home, self.shell)
#--------------------------------------------------------------------------------------
parser = argparse.ArgumentParser(description="""mostra  lineas de un fitxer o stdin""",\
        prog="exemple.py",epilog="thats all folks")
parser.add_argument("-f","--fitxer",type=str,help="fitxers /etc/passwd  a procesar default stdin",metavar="elfitxer",dest="fitxer")
args=parser.parse_args()
#print args
#--------------------------------------------------------------------------------------
fileIn=open(args.fitxer,'r')
listaUsuaris=[]
for line in fileIn:
	usuari = UnixUser(line)
	listaUsuaris.append(usuari)
fileIn.close()

for user in listaUsuaris:
	#print user.__str__()
	print user

exit(0)

