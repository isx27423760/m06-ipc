#!/usr/bin/env python
# -*- coding: utf-8 -*-
#@ edt asix m06 curs 2018-2019
# nom : Franlin colque
# curs : hisx2
#exercici : 9
# descripcio : EXemple de creacio de objectes i funcions per a users i ordenarlos
# 09-sort-users.py   [-s login|gid|gname]  -u fileusers -g fileGroup
#----------------------------------------------------------------------------
import argparse,sys
#class unixgroup
class UnixUser():
	"""Classe UnixUser: prototipus de /etc/passwd
	login:passwd:uid:gid:gecos:homeDirectory:shell"""
	def __init__(self,userLine):
		"""constructor objectes UnixUser"""
		userFile=userLine.split(":")
		#camps
		self.login=userFile[0]
		self.password=userFile[1]
		self.uid=int(userFile[2])
		self.gid=int(userFile[3])
		self.gname=""
		self.gecos=userFile[4]
		self.home=userFile[5]
		self.shell=userFile[6]
	def __str__(self):
		"""functió to_string"""
		return "%s %s %d %d %s %s %s" % (self.login, self.password, self.uid, \
		self.gid, self.gecos, self.home, self.shell)
#class unixGroup
class UnixGroup():
	"""Classe UnixGroup: prototipus de /etc/group
	gname:password:gid:users_list"""
	def __init__(self,groupLine):
		"""Constructor objectes UnixGroup"""
		groupFile=groupLine.split(":")
		#camps
		self.gname=groupFile[0]
		self.password=groupFile[1]
		self.gid=int(groupFile[2])
		self.userlist=groupFile[3].split(',')
	def __str__(self):
		"""functió to_string"""
		return "%s %s %d %s" % (self.gname, self.password, self.gid, self.userlist)

#Ordenacio segons criteri
def cmp_login(elem1,elem2):
	'''
	criteri de comparacio de una llista per login
	'''
	if elem1.login > elem2.login:
		result = 1
	elif elem1.login < elem2.login:
		result = -1
	else:
		result = 0
	return result

def cmp_gid(elem1,elem2):
	'''
	criteri de comparacio de una llista per gid 4 camp, i desempatat per login
	'''
	if elem1.gid > elem2.gid:
		result = 1
	elif elem1.gid < elem2.gid:
		result = -1
	elif elem1.login > elem2.login:
		result = 1 
	elif elem1.login < elem2.login:
		result = -1
	return result
	
#--------------------------------------------------------------------------------------
parser = argparse.ArgumentParser(description="""mostra  lineas de un fitxer o stdin""",\
        prog="exemple.py",epilog="thats all folks")

parser.add_argument("-s","--sort",type=str,help="ordenar per criteri login o gid",metavar="elsort",dest="criteri",default="login",\
					choices=['login','gid','gname'])

parser.add_argument("-u","--fileusers",type=str,help="fitxer a procesar de users",metavar="elfitxeruser")

parser.add_argument("-g","--filegroups",type=str,help="fitxer a procesar de  dic de groups",metavar="elfitxergroup")

args=parser.parse_args()
#print args
#-------------------------------------------------------------------------------------------------
fileInGroups=open(args.filegroups,'r')
groupDic={}
for line in fileInGroups:
	groups = UnixGroup(line)
	groupDic[groups.gid]=groups
fileInGroups.close()

fileInUsers=open(args.fileusers,'r')
listaUsuaris=[]
for line in fileInUsers:
	users = UnixUser(line)
	if users.gid in groupDic:
		users.gname=groupDic[users.gid].gname
	listaUsuaris.append(users)
fileInUsers.close()


for user in listaUsuaris:
	print user


# ~ print listaUsuaris

# ~ for k,v in groupDic.items():
	# ~ for i in listaUsuaris:
		# ~ if str(i.gid) == k:
			# ~ listaUsuaris.append(v[0].split(" ")[0])
			# ~ #print v[0].split(" ")[0],' ',k,' ',i.gid

# ~ listaUsuaris.__str__()











# ~ for k,v in groupDic.items():
	# ~ print v[0].split(" ")[0]
	# ~ for i in listaUsuaris:
		# ~ if 
