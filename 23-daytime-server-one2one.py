# /usr/bin/python
#-*- coding: utf-8-*-
#
# daytime-server.py  
# ---------------------------------------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# Descripcio : 23 
# ---------------------------------------------------------------------
import sys,socket
from subprocess import Popen, PIPE
HOST = ''
PORT = 50001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#reutiliza el port
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
s.listen(1)
while True:
	conn, addr = s.accept()
	print "Connected by", addr
	command = ["date"]
	pipeData = Popen(command,stdout=PIPE)
	#finalitzara cuan el popen finalitza
	for line in pipeData.stdout:
		conn.send(line)
	conn.close()
sys.exit(0)
