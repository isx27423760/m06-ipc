# /usr/bin/python
#-*- coding: utf-8-*-
#
# daytime-server.py  
# ---------------------------------------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# Descripcio : 24-calendar-server-one2one.py [ -s server]  [-p port]
# ---------------------------------------------------------------------
import sys,socket,argparse
from subprocess import Popen, PIPE
#--------------------------------------------------------------------
parser = argparse.ArgumentParser(description="""Exemple popen de cal""")
parser.add_argument("-s","--server",type=str,help="server a conectare",metavar="server",default='')
parser.add_argument("-p","--port",type=int,help="port del servidor",metavar="port",default=50001)
args=parser.parse_args()
print args
#-----------------------------------------------------------------------
HOST = ''
PORT = args.port
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))
while True:
  data = s.recv(1024)
  if not data: break
  print repr(data)
s.close()
sys.exit(0)
