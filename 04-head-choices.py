#!/usr/bin/env python
# -*- coding: utf-8 -*-
#@ edt asix m06 curs 2018-2019
# nom : Franlin colque
# curs : hisx2
# descripcio : Programa que mostra lineas
# Usage: prog.py [-n 5 | 10 | 15] file
#   
#-----------------------------------------------------------------------
import argparse,sys

parser = argparse.ArgumentParser(description="""mostrar 5 10 o 15 lineas de un fitxer o stdin""",\
        prog="exemple.py",epilog="thats all folks")

parser.add_argument("-n","--num",type=int,help="num de lineas  5|10|15",dest="numlinea",metavar="numlineas",choices=[5,10,15],default=10)

parser.add_argument("fitxer",type=str,help="fitxer a procesar",metavar="elfitxer")

args=parser.parse_args()
print args
#-------------------------------------
MAX=args.numlinea
fileIn=open(args.fitxer,'r')
conter = 0
for item in fileIn:
    conter += 1
    print item,
    if conter == MAX :
        break
fileIn.close()
exit(0)
