#!/usr/bin/env python
# -*- coding: utf-8 -*-
#@ edt asix m06 curs 2018-2019
# nom : Franlin colque
# curs : hisx2
# descripcio : Programa que mostra les 10 primeras lineas
# Usage: head [file]
#   10 lineas , file o stdin
#-----------------------------------------------------------------------
import argparse
parser = argparse.ArgumentParser(description="""exemple de processar arguments""",\
        prog="exemple.py",epilog="thats all folks")
parser.add_argument("-e","--edat",type=int,help="edat (int)",dest="useredat",metavar="laedat")

parser.add_argument("fit",type=str,help="fitxer",metavar="elfitxer")

#parser.add_argument("--verbosity",help="ser verbose")

args=parser.parse_args()
print parser
print args

exit(0)
