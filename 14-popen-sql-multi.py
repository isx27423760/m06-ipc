#!/usr/bin/env python
# -*- coding: utf-8 -*-
#@ edt asix m06 curs 2018-2019
# nom : Franlin colque
# curs : hisx2
#exercici : 14
#----------------------------------------------------------------------------
import sys, argparse
from subprocess import Popen, PIPE
#--------------------------------------------------------------------------
parser = argparse.ArgumentParser(description="""Exemple popen select de sql""")
parser.add_argument("-d","--database",type=str,help="base de dades a consultar",dest="datebase")
parser.add_argument("-c","--numclie",type=str,help="numclies a procesar",metavar="elclie",dest="client",action='append')
args=parser.parse_args()

#----------------------------------------------------------------------------
command = "psql -qtA -F',' -h 172.17.0.2 -U edtasixm06 "+ args.datebase
pipeData = Popen(command,shell = True, stdin=PIPE, stdout=PIPE, stderr=PIPE)
for num_clie in args.client:
	ordreSQL="select * from clientes where num_clie=%s;" % (num_clie)
	pipeData.stdin.write(ordreSQL+"\n")
	print pipeData.stdout.readline()

pipeData.stdin.write("\q\n")
sys.exit(0)
