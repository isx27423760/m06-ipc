#!/usr/bin/env python
# -*- coding: utf-8 -*-
#@ edt asix m06 curs 2018-2019
# nom : Franlin colque
# curs : hisx2
#exercici : signal example 
#----------------------------------------------------------------------------
import sys,os,signal

def myhandler(signum,frame):
	print "Signal handler called with signal:", signum
	print "hasta luego lucas!"
	sys.exit(1)

def mydeath(signum,frame):
	print "Signal handler called with signal:", signum
	print "que te mueras tu!"

#cuan pasa un sigalarm disparas la funcio handler 
signal.signal(signal.SIGALRM,myhandler)
signal.signal(signal.SIGUSR2,myhandler)
signal.signal(signal.SIGUSR1,mydeath)
#cuan pasa un sigterm disparas la funcio handler 
signal.signal(signal.SIGTERM,signal.SIG_IGN)
#ignora ctr+c
signal.signal(signal.SIGINT,signal.SIG_IGN)

#posa 60 segons de alarma
signal.alarm(180)
print os.getpid()

while True:
	#fem un open que es queda encallat
	pass
signal.alarm(0)
sys.exit(0)





# ~ 6978 pts/2    R+     0:04 python 15-example-signal.py
# ~ 6979 pts/3    R+     0:00 ps -ax
# ~ [isx27423760@i12 ipc-2018-19]$ kill -15 6978
# ~ [isx27423760@i12 ipc-2018-19]$ kill -15 6978
# ~ [isx27423760@i12 ipc-2018-19]$ kill -14 6978

# ~ isx27423760@i12 m06-ipc]$ python 15-example-signal.py 
# ~ 6978
# ~ Signal handler called with signal: 15
# ~ que te mueras tu!
# ~ Signal handler called with signal: 15
# ~ que te mueras tu!
# ~ Signal handler called with signal: 14
# ~ hasta luego lucas!

