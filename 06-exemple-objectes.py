#!/usr/bin/env python
# -*- coding: utf-8 -*-
#@ edt asix m06 curs 2018-2019
# nom : Franlin colque
# curs : hisx2
# descripcio : EXemple de creacio de objectes
#----------------------------------------------------------------------------
class UnixUser():
	"""Classe UnixUser: prototipus de /etc/passwd
	login:passwd:uid:gid:gecos:homeDirectory:shell"""
	def __init__(self,l,u,g):
		"constructor objectes UnixUser"
		self.login=l
		self.uid=u
		self.gid=g
	def __init__(self):
		self.login="unknown"
		self.uid=1000
		self.gid=100
	def show(self):
		print "login: %s, uid:%d ,gid: %d" % (self.login,self.uid,self.gid)
	def sumaun(self):
		self.uid+=1

user1=UnixUser("pere",15,100)
user1.show()
user1.sumaun()
