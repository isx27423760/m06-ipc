#!/usr/bin/env python
# -*- coding: utf-8 -*-
#@ edt asix m06 curs 2018-2019
# nom : Franlin colque
# curs : hisx2
#exercici : 16 
#-----------------------------------------------------------------------
import sys,os,signal, argparse

parser = argparse.ArgumentParser(description="""Exemple de larma""")
parser.add_argument("segons",type=int,help="base de dades a consultar")
args=parser.parse_args()
print args
#-----------------------------------------------------------------------
global upp
global down
upp = 0
down = 0
def mes1(signum,frame):
	global upp	
	print "Signal ms1 USER1 called with signal:", signum
	upp += 1 
	signal.alarm(signal.alarm(0)+60)

def menys1(signum,frame):
	global down
	print "Signal mesnys 1 USR2 called with signal:", signum
	status=signal.alarm(0)
	if status-60 < 0:
		print "No se puede restar: %d" % (status)
	else:
		down += 1
		signal.alarm(status-60)

def myhup(signum,frame):
	print "Signal handler called with signal:", signum
	signal.alarm(args.segons)

def mysigterm(signum,frame):
	print "my sigterm:", signum
	restant=signal.alarm(0)
	signal.alarm(restant)
	print "Quan falta : %d   " % (restant)
	
def mydeath(signum,frame):
	print "Signal handler called with signal:", signum
	print "hasta luego lucas! :  upp  : %d    ... down :  %d   ." % (upp,down)
	sys.exit(1)

#aumenta en un 1 minut
signal.signal(signal.SIGUSR1,mes1)
#desminuex en 1 minut
signal.signal(signal.SIGUSR2,menys1)
#hup reinicialitza 
signal.signal(signal.SIGHUP,myhup)
#cuan pasa un sigterm disparas la funcio handler 
signal.signal(signal.SIGTERM,mysigterm)
#cuan pasa un sigalarm disparas la funcio  i plega
signal.signal(signal.SIGALRM,mydeath)
#ignora ctr+c
signal.signal(signal.SIGINT,signal.SIG_IGN)

#posa els segons de la alarma com a argument
signal.alarm(args.segons)
print os.getpid()

while True:
	#fem un open que es queda encallat
	pass
signal.alarm(0)
sys.exit(0)
