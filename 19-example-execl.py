#!/usr/bin/env python
# -*- coding: utf-8 -*-
#@ edt asix m06 curs 2018-2019
# nom : Franlin colque
# curs : hisx2
#exercici : 18-example-execl.py mes examples
#-----------------------------------------------------------------------
import sys,os,signal
print "Hola, començament del programa principal"
print "PID pare: ", os.getpid()
#crea fills
pid=os.fork()
if pid !=0:
  #os.wait()  --> espera el programa pare
  print "Programa Pare", os.getpid(), pid
  print "Hasta lugo lucas!"
  sys.exit(0)

#el programa fill 
print "Programa fill", os.getpid(), pid
#carrega en el proces actual el ls 
#os.execv("/usr/bin/ls",["/usr/bin/ls","-ls","/"])
#os.execl("/usr/bin/ls","/usr/bin/ls","-la","/")
#os.execlp("ls","ls","-la","/")
os.execve("/usr/bin/ls",["/usr/bin/ls","-la","/"],{'nom':'/tmp','edat':'15'})
#no se exectura mai el print final, perque ja no existeixe , desapareix
print "Hasta lugo lucas!"
sys.exit(0)
