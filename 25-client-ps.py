# /usr/bin/python
#-*- coding: utf-8-*-
#
# 25-client.py  
# ---------------------------------------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# nom : Franlin colque
# curs : hisx2
#Descripcio: 25-client.py
# ----------------------------------------------------------------
import sys,socket,os,signal,argparse
from subprocess import Popen, PIPE

parser = argparse.ArgumentParser(description="""Exemple PS client""")
parser.add_argument("-p","--port",type=int,default=50001)
parser.add_argument("-s","--server",type=str,dest="server")
args=parser.parse_args()

HOST = ''
PORT = 50001

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))
command = ["ps -ax"]
pipeData = Popen(command,stdout=PIPE,shell=True)
for line in pipeData.stdout:
	s.send(line)
s.close()
sys.exit(0)
