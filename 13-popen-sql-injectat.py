#!/usr/bin/env python
# -*- coding: utf-8 -*-
#@ edt asix m06 curs 2018-2019
# nom : Franlin colque
# curs : hisx2
#exercici : 13-popen-sql-injectat.py "select"
#----------------------------------------------------------------------------
import sys, argparse
from subprocess import Popen, PIPE
#--------------------------------------------------------------------------
parser = argparse.ArgumentParser(description=\
        """Exemple popen select de sql""")
parser.add_argument("select",type=str,\
        help="taula a mostrat")
args=parser.parse_args()
#----------------------------------------------------------------------------
command = ["psql -qtA -F',' -h 172.17.0.2 -U edtasixm06 training "]
pipeData = Popen(command,shell = True, stdin=PIPE, stdout=PIPE, stderr=PIPE)
pipeData.stdin.write(args.select+"\n\q\n")

for line in pipeData.stdout:
    print line,
exit(0)
