# /usr/bin/python
#-*- coding: utf-8-*-
#
# daytime-server.py  
# ---------------------------------------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# Descripcio : 24-calendar-server-one2one.py [ -p port]  [-a any]
# ---------------------------------------------------------------------
import sys,socket,argparse,os,signal
from subprocess import Popen, PIPE
#--------------------------------------------------------------------
global peers
global upp
peers = []
upp = 0
def mysigusr1(signum,frame):
	global peers
	print "Signal handler called with signal:", signum 
	print "conexions establertes : ",peers
	sys.exit(1)

def mysigusr2(signum,frame):
	global upp
	print "Signal handler called with signal:", signum
	print "num conexions : %d " % (upp)
	sys.exit(2)

def mysigterm(signum,frame):
	print "Signal handler called with signal:", signum
	print "peers: ",peers
	print "count : %d" % (upp)
	print "Hasta luego Lucas!!"
	sys.exit(3)

#---------------------------------------------------------------
parser = argparse.ArgumentParser(description="""Exemple popen de cal""")
parser.add_argument("-p","--port",type=int,help="port del servidor",metavar="port",default=50001)
parser.add_argument("-a","--any",type=int,help="any de mostrar el cal",metavar="any",default=2019)
args=parser.parse_args()
print args
#---------------------------------------------------------------
print "Hola, començament del programa principal"
print "PID pare: ", os.getpid()
pid=os.fork()
if pid !=0:
	print "Programa Pare", os.getpid(), pid
	print "Hasta lugo lucas!"
	sys.exit(0)
print "Programa fill", os.getpid(), pid
signal.signal(signal.SIGUSR1,mysigusr1)
signal.signal(signal.SIGUSR2,mysigusr2)
signal.signal(signal.SIGTERM,mysigterm)
HOST = ''
PORT = args.port
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#reutiliza el port
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
s.listen(1)
while True:
	conn, addr = s.accept()
	peers += [addr]
	upp += 1
	cmd ='/usr/bin/cal '+str(args.any)
	#cmd ='/usr/bin/cal'
	pipeData = Popen(cmd,shell=True,stdin=PIPE,stdout=PIPE,stderr=PIPE)
	#finalitzara cuan el popen finalitza
	for line in pipeData.stdout: 
		conn.send(line)
	conn.close()
sys.exit(0)

