#!/usr/bin/env python
# -*- coding: utf-8 -*-
#@ edt asix m06 curs 2018-2019
# nom : Franlin colque
# curs : hisx2
#exercici : 8
# descripcio : EXemple de creacio de objectes i funcions per a users i ordenarlos
#08-sort-users.py [-s login|gid] file
#----------------------------------------------------------------------------
import argparse,sys
#clase unixUser
class UnixUser():
	"""Classe UnixUser: prototipus de /etc/passwd
	login:passwd:uid:gid:gecos:homeDirectory:shell"""
	def __init__(self,userLine):
		"""constructor objectes UnixUser"""
		userFile=userLine.split(":")
		#camps
		self.login=userFile[0]
		self.password=userFile[1]
		self.uid=int(userFile[2])
		self.gid=int(userFile[3])
		self.gecos=userFile[4]
		self.home=userFile[5]
		self.shell=userFile[6]
	def __str__(self):
		"""functió to_string"""
		return "%s %s %d %d %s %s %s" % (self.login, self.password, self.uid, \
		self.gid, self.gecos, self.home, self.shell)

#Ordenacio segons criteri
def cmp_login(elem1,elem2):
	'''
	criteri de comparacio de una llista per login
	'''
	if elem1.login > elem2.login:
		result = 1
	elif elem1.login < elem2.login:
		result = -1
	else:
		result = 0
	return result

def cmp_gid(elem1,elem2):
	'''
	criteri de comparacio de una llista per gid 4 camp, i desempatat per login
	'''
	if elem1.gid > elem2.gid:
		result = 1
	elif elem1.gid < elem2.gid:
		result = -1
	elif elem1.login > elem2.login:
		result = 1 
	elif elem1.login < elem2.login:
		result = -1
	return result
	
#--------------------------------------------------------------------------------------
parser = argparse.ArgumentParser(description="""mostra  lineas de un fitxer o stdin""",\
        prog="exemple.py",epilog="thats all folks")

parser.add_argument("-s","--sort",type=str,help="ordenar per criteri login o gid",metavar="elsort",dest="criteri")

parser.add_argument("fitxer",type=str,help="fitxer a procesar",metavar="elfitxer")

args=parser.parse_args()
#print args
#--------------------------------------------------------------------------------------
fileIn=open(args.fitxer,'r')
listaUsuaris=[]
for line in fileIn:
	usuari = UnixUser(line)
	listaUsuaris.append(usuari)
fileIn.close()
#ordena per login
if args.criteri=="login":
	listaUsuaris.sort(cmp=cmp_login)
#ordena per gid
elif args.criteri=="gid":
	listaUsuaris.sort(cmp=cmp_gid)

for user in listaUsuaris:
	print user

exit(0)

