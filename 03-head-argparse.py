#!/usr/bin/env python
# -*- coding: utf-8 -*-
#@ edt asix m06 curs 2018-2019
# nom : Franlin colque
# curs : hisx2
# descripcio : Programa que mostra les 10 primeras lineas
# Usage: prog.py [-n 15] [-f file]
#   
#-----------------------------------------------------------------------
import argparse,sys

parser = argparse.ArgumentParser(description="""mostrar n lineas de un fitxer o stdin""",\
        prog="exemple.py",epilog="thats all folks")

parser.add_argument("-n","--num",type=int,help="edat (int)",dest="numlinea",metavar="numlineas",default=10)

parser.add_argument("-f","--fitxer",type=str,help="fitxer a procesar default stdin",metavar="elfitxer",default="/dev/stdin",dest="fitxer")

args=parser.parse_args()
print args
#-------------------------------------
MAX=args.numlinea
fileIn=open(args.fitxer,'r')
conter = 0
for item in fileIn:
    conter += 1
    print item,
    if conter == MAX :
        break
fileIn.close()
exit(0)
