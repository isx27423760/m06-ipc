#!/usr/bin/env python
# -*- coding: utf-8 -*-
#@ edt asix m06 curs 2018-2019
# nom : Franlin colque
# curs : hisx2
# descripcio : Programa que mostra les 10 primeras lineas
# Usage: head [file]
#   10 lineas , file o stdin
#-----------------------------------------------------------------------
import sys

MAX=2 
fileIn=sys.stdin
if len(sys.argv)==2:
    fileIn=open(sys.argv[1],'r')
i = 0
for item in fileIn:
    i += 1
    print item,
    if i == MAX :
        break
fileIn.close()
sys.exit(0)
