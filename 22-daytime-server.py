# /usr/bin/python
#-*- coding: utf-8-*-
#
# 22-daytime-server.py  
# -------------------------------------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# nom : Franlin colque
# curs : hisx2
#Descripcio: cuan es conecta un cliente li envia la data i tanca la conexio
# -------------------------------------------------------------------
import sys,socket,argparse
from subprocess import Popen, PIPE
 
HOST = ''
PORT = 50001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
s.listen(1)
conn, addr = s.accept()
print "Connected by" , addr
while True:
	command = ["/usr/bin/date"]
	pipeData = Popen(command,shell=True,stdout=PIPE)
	fecha = pipeData.stdout.readline()
	if not fecha: break
	conn.send(fecha)
	exit(1)
conn.close()
sys.exit(0)

