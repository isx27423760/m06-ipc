#!/usr/bin/env python
# -*- coding: utf-8 -*-
#@ edt asix m06 curs 2018-2019
# nom : Franlin colque
# curs : hisx2
#exercici : 9
# descripcio : EXemple de creacio de objectes i funcions per a users i ordenarlos
# 10-count-by-group.py [-s gid | gname | nusers ] -u usuaris -g grups
#----------------------------------------------------------------------------
import sys, argparse
groupDict={}
parser = argparse.ArgumentParser(description=\
        """Llistar els usuaris de file o stdin (format /etc/passwd""",\
        epilog="thats all folks")
parser.add_argument("-s","--sort",type=str,\
        help="sort criteria:  gid | gname | nusers", metavar="criteria",\
        choices=["gid","gname","nusers"],dest="criteria")
parser.add_argument("-u","--userFile",type=str,\
        help="user file (/etc/passwd style)", metavar="userFile")
parser.add_argument("-g","--groupFile",type=str,\
        help="user file (/etc/group style)", metavar="groupFile")
args=parser.parse_args()
# -------------------------------------------------------
class UnixUser():
  """Classe UnixUser: prototipus de /etc/passwd
  login:passwd:uid:gid:gecos:home:shell"""
  def __init__(self,userLine):
    "Constructor objectes UnixUser"
    userField=userLine.split(":")
    self.login=userField[0]
    self.passwd=userField[1]
    self.uid=int(userField[2])
    self.gid=int(userField[3])
    self.gname=""
    if self.gid in groupDict:
      self.gname=groupDict[self.gid].gname
    self.gecos=userField[4]
    self.home=userField[5]
    self.shell=userField[6]
  def __str__(self):
    "functió to_string d'un objcete UnixUser"
    return "%s %d %d %s" % (self.login, self.uid, self.gid, self.gname)
# -------------------------------------------------------
class UnixGroup():
  """Classe UnixGroup: prototipus de /etc/group
  gname_passwd:gid:listUsers"""
  def __init__(self,groupLine):
    "Constructor objectes UnixGroup"
    groupField = groupLine.split(":")
    self.gname = groupField[0]
    self.passwd = groupField[1]
    self.gid = int(groupField[2])
    self.userListStr = groupField[3]
    self.userList=[]
    if self.userListStr[:-1]:
      self.userList = self.userListStr[:-1].split(",")
  def __str__(self):
    "functió to_string d'un objecte UnixGroup"
    return "%s %d %s" % (self.gname, int(self.gid), self.userList)
# -------------------------------------------------------
groupFile=open(args.groupFile,"r")
for line in groupFile:
  group=UnixGroup(line)
  groupDict[group.gid]=group
groupFile.close()

userFile=open(args.userFile,"r")
userList=[]
for line in userFile:
  user=UnixUser(line)
  userList.append(user)
  if user.gid in groupDict:
	  if user.login not in groupDict[user.gid].userList:
		  groupDict[user.gid].userList.append(user.login)
userFile.close()

index=[]
if args.criteria=="gid":
	index = [ k for k in groupDict]
elif args.criteria=="gname":
	index = [ (groupDict[k].gname,k) for k in groupDict ]
else :
	index = [ (len(groupDict[k].userList),k) for k in groupDict ]

index.sort()

print index

if args.criteria!="gid":
    for i,k in index:
        print groupDict[k]
else:
    for k in index:
        print groupDict[k]
    

exit(0)

